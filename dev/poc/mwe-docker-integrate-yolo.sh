docker pull kitware/viame:gpu-algorithms-sam

# This needs to point to a local checkout of bioharn
LOCAL_BIOHARN_DPATH=$HOME/code/bioharn
LOCAL_VIAME_DPATH=$HOME/code/VIAME
LOCAL_YOLO_DPATH=$HOME/code/YOLO-v9

docker run --gpus=all \
    --shm-size=8g \
    --volume "$LOCAL_BIOHARN_DPATH:/host-bioharn" \
    --volume "$LOCAL_VIAME_DPATH:/host-viame" \
    --volume "$LOCAL_YOLO_DPATH:/host-yolo" \
    -it kitware/viame:gpu-algorithms-sam \
    bash


# Setup the VIAME Python environment
cd /opt/noaa/viame/
source setup_viame.sh

# Developer convience
apt install fd-find tree
python -m pip install ipython
python -m pip install xdev

# Clone the local copy of bioharn (could also clone from the gitlab repo)
git config --global --add safe.directory /host-yolo/.git
git config --global --add safe.directory /host-viame/.git
git config --global --add safe.directory /host-bioharn/.git
mkdir -p /root/code
git clone /host-yolo/.git /root/code/YOLO-v9
git clone /host-bioharn/.git /root/code/bioharn
git clone /host-viame/.git /root/code/VIAME


diff /root/code/VIAME/plugins/pytorch/netharn_trainer.py /opt/noaa/viame/lib/python3.10/site-packages/viame/arrows/pytorch/netharn_trainer.py


# Update certain files in VIAME to their latest state
# Install the updated bioharn code and dependencies
python -m pip install ndsampler netharn -U
python -m pip uninstall bioharn -y
cd /root/code/bioharn
python -m pip install -e .
cd /root/code/YOLO-v9
python -m pip install -e .


# Test a netharn detector
cd /opt/noaa/viame/examples/object_detector_training
viame_train_detector \
  -i training_data_mouss \
  -c /opt/noaa/viame/configs/pipelines/train_detector_netharn_mask_rcnn_720.conf \
  --threshold 0.0



# Hack in certain files directly from the host to the installed VIAME
ln -sf /host-viame/plugins/pytorch/mit_yolo_trainer.py /opt/noaa/viame/lib/python3.10/site-packages/viame/arrows/pytorch/mit_yolo_trainer.py
ln -sf /host-viame/plugins/pytorch/_utils.py /opt/noaa/viame/lib/python3.10/site-packages/viame/arrows/pytorch/_utils.py
ln -sf /host-viame/plugins/pytorch/kwcoco_train_detector.py /opt/noaa/viame/lib/python3.10/site-packages/viame/arrows/pytorch/kwcoco_train_detector.py
ln -sf /host-viame/configs/pipelines/train_detector_mit_yolo_640.conf /opt/noaa/viame/configs/pipelines/train_detector_mit_yolo_640.conf


# Test the hacked in YOLO detector
cd /opt/noaa/viame/examples/object_detector_training
viame_train_detector \
  -i /opt/noaa/viame/examples/object_detector_training/training_data_mouss \
  -c /opt/noaa/viame/configs/pipelines/train_detector_mit_yolo_640.conf \
  --threshold 0.0
