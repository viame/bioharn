docker pull kitware/viame:gpu-algorithms-sam

# This needs to point to a local checkout of bioharn
LOCAL_BIOHARN_DPATH=$HOME/code/bioharn
LOCAL_VIAME_DPATH=$HOME/code/VIAME

docker run --gpus=all \
    --volume "$LOCAL_BIOHARN_DPATH:/host-bioharn" \
    --volume "$LOCAL_VIAME_DPATH:/host-viame" \
    -it kitware/viame:gpu-algorithms-sam \
    bash


# Setup the VIAME Python environment
cd /opt/noaa/viame/
source setup_viame.sh

python -m pip install ipython

# Apply a patch directly to site packages
cd /opt/noaa/viame/lib/python3.10/site-packages/viame/arrows/
patch -p1 <<'EOF'
--- a/pytorch/netharn_detector.py
+++ b/pytorch/netharn_detector.py
@@ -272,7 +272,11 @@ def _kwimage_to_kwiver_detections(detections):
     if 'segmentations' in detections.data:
         segmentations = detections.data['segmentations']

-    boxes = detections.boxes.to_tlbr()
+    try:
+        boxes = detections.boxes.to_ltrb()
+    except Exception:
+        boxes = detections.boxes.to_tlbr()
+
     scores = detections.scores
     class_idxs = detections.class_idxs
EOF

cd /opt/noaa/viame/

# Remove the pre-installed bioharn
python -m pip uninstall bioharn

# Clone the local copy of bioharn (could also clone from the gitlab repo)
git config --global --add safe.directory /host-bioharn/.git
git clone /host-bioharn/.git bioharn-repo

# Install the new clone
cd /opt/noaa/viame/bioharn-repo
python -m pip install -e .

# Setup demo Data
cd /opt/noaa/viame/bioharn-repo
python -c "if 1:
    import kwimage
    data = kwimage.checkerboard()
    kwimage.imwrite('checkers.jpg', data)
"
echo "checkers.jpg" >> input_image_list_small_set.txt

# Call the Script
bash /opt/noaa/viame/examples/object_detection/run_fish_without_motion.sh











cat /opt/noaa/viame/examples/object_detection/run_fish_without_motion.sh


bash /opt/noaa/viame/examples/object_detection/run_fish_without_motion.sh

python -c "if 1:
import kwimage
data = kwimage.checkerboard()
kwimage.imwrite('checkers.jpg', data)
"
echo "checkers.jpg" >> input_image_list_small_set.txt
bash /opt/noaa/viame/examples/object_detection/run_fish_without_motion.sh
