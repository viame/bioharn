#!/bin/bash
__doc__="
Trained on namek
"
export CUDA_VISIBLE_DEVICES="0,"
#export CUDA_VISIBLE_DEVICES=0
DVC_DATA_DPATH=$HOME/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH
DVC_EXPT_DPATH=$HOME/data/dvc-repos/viame_dvc/experiments
WORKDIR=$DVC_EXPT_DPATH/training/$HOSTNAME/$USER

DATASET_CODE=HABCAM-FISH
EXPERIMENT_NAME="viame2024-train_v007_detectron2_regnety_noscallop_fixsplit"
KWCOCO_BUNDLE_DPATH=$DVC_DATA_DPATH
DEFAULT_ROOT_DIR=$WORKDIR/$DATASET_CODE/runs/$EXPERIMENT_NAME

TRAIN_FPATH=$KWCOCO_BUNDLE_DPATH/train-v06-noscallop.kwcoco.zip
VALI_FPATH=$KWCOCO_BUNDLE_DPATH/val-v06-noscallop.kwcoco.zip
TEST_FPATH=$KWCOCO_BUNDLE_DPATH/test-v06-noscallop.kwcoco.zip
echo "TRAIN_FPATH = $TRAIN_FPATH"
echo "VALI_FPATH = $VALI_FPATH"
echo "DEFAULT_ROOT_DIR = $DEFAULT_ROOT_DIR"

show_stats(){
    kwcoco stats "$TRAIN_FPATH" "$VALI_FPATH" "$TEST_FPATH"
}

mkdir -p "$DEFAULT_ROOT_DIR"

echo "
default_root_dir: $DEFAULT_ROOT_DIR
expt_name: $EXPERIMENT_NAME
train_fpath: $TRAIN_FPATH
vali_fpath: $VALI_FPATH
base: new_baselines/mask_rcnn_regnety_4gf_dds_FPN_400ep_LSJ.py
init: new_baselines/mask_rcnn_regnety_4gf_dds_FPN_400ep_LSJ.py
cfg:
    dataloader:
        train:
            total_batch_size: 12
            num_workers: 4
            #mapper:
            #    use_instance_mask: true
            #    recompute_boxes: false
    #model:
    #    roi_heads:
    #        mask_in_features: null
    #        mask_pooler: null
    #        mask_head: null
    optimizer:
        lr: 0.001
    train:
        amp:
            enabled: true
        max_iter: 184375
        eval_period: 5000
        log_period: 20
        checkpointer:
            period: 5000
            max_to_keep: 100
        device: cuda
" > "$DEFAULT_ROOT_DIR"/train_config.yaml
cat "$DEFAULT_ROOT_DIR"/train_config.yaml
python -m geowatch.tasks.detectron2.fit --config "$DEFAULT_ROOT_DIR"/train_config.yaml

# TODO: ensure fit does this
kwcoco info "$TRAIN_FPATH" -c inf | jq .categories > "$DEFAULT_ROOT_DIR"/v_0042349f/categories.json


# Check which models exist
DATASET_CODE=HABCAM-FISH
EXPERIMENT_NAME="viame2024-train_v007_detectron2_regnety_noscallop_fixsplit"
KWCOCO_BUNDLE_DPATH=$DVC_DATA_DPATH
DEFAULT_ROOT_DIR=$WORKDIR/$DATASET_CODE/runs/$EXPERIMENT_NAME
ls "$DEFAULT_ROOT_DIR"/*/model_*.pth

python -m geowatch.tasks.detectron2.predict \
    --checkpoint_fpath /home/joncrall/data/dvc-repos/viame_dvc/experiments/training/namek/joncrall/HABCAM-FISH/runs/viame2024-train_v007_detectron2_regnety_noscallop_fixsplit/v_0042349f/model_0179999.pth \
    --base "auto" \
    --nms_thresh 0.5 \
    --src_fpath "$TEST_FPATH" \
    --dst_fpath "$DEFAULT_ROOT_DIR"/oneoff_evaluate/pred.kwcoco.json \
    --workers=4

kwcoco eval_detections \
    --true_dataset "$TEST_FPATH" \
    --pred_dataset "$DEFAULT_ROOT_DIR"/oneoff_evaluate/pred.kwcoco.json \
    --out_dpath "$DEFAULT_ROOT_DIR"/oneoff_evaluate/coco_metrics \
    --confusion_fpath "$DEFAULT_ROOT_DIR"/oneoff_evaluate/confusion.kwcoco.json \


geowatch visualize "$DEFAULT_ROOT_DIR"/oneoff_evaluate/confusion.kwcoco.json --smart --animate=False --channels="red|green|blue,red|green|blue" --role_order="true,pred"
