2025-01-24
----------

Ugg, the v04 (no scallops no masks) and v05 (no scallops with SAM2 masks)
splits are not comparable. I can evaluate on a subset of the test data, but
its still not comparable as it should be.
