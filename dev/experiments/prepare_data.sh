#!/bin/bash
__doc__="
See Also:
    ~/code/bioharn/dev/data_tools/convert_cff_to_kwcoco.py
"
#export CUDA_VISIBLE_DEVICES=0
DVC_DATA_DPATH=$HOME/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH
KWCOCO_BUNDLE_DPATH=$DVC_DATA_DPATH
RAW_DSET=$KWCOCO_BUNDLE_DPATH/data.kwcoco.zip

if [ ! -f "$RAW_DSET" ]; then

    python ~/code/bioharn/dev/data_tools/convert_cff_to_kwcoco.py \
        --in_fpath "$DVC_DATA_DPATH"/habcam-2020-2021.csv \
        --out_fpath "$DVC_DATA_DPATH"/data.kwcoco.zip

    # TODO: add CLI that can infer / assume channel specs
    kwcoco conform "$RAW_DSET" --inplace=True --legacy=True --mmlab=True --workers=16

    kwcoco fixup "$RAW_DSET" --inplace=True --corrupted_assets=only_shape --workers=0

fi

cd "$KWCOCO_BUNDLE_DPATH"
FULL_DSET=$KWCOCO_BUNDLE_DPATH/data-with-polys.kwcoco.zip

show_stats(){
    kwcoco stats "$FULL_DSET"
}

TRAIN_FPATH=$KWCOCO_BUNDLE_DPATH/train-v04.kwcoco.zip
VALI_FPATH=$KWCOCO_BUNDLE_DPATH/vali-v04.kwcoco.zip
TEST_FPATH=$KWCOCO_BUNDLE_DPATH/test-v04.kwcoco.zip

if [ -f "$TRAIN_FPATH" ] ; then
    echo "Splits already exist"
else
    kwcoco split "$FULL_DSET" --rng 10621406 --dst1 "$KWCOCO_BUNDLE_DPATH/learn-v01.kwcoco.zip" --dst2 "$TEST_FPATH"
    kwcoco split "$KWCOCO_BUNDLE_DPATH/learn-v01.kwcoco.zip" --rng 10631407 --dst1 "$TRAIN_FPATH" --dst2 "$VALI_FPATH"

    kwcoco conform "$TRAIN_FPATH" --inplace=True --workers=16
    kwcoco conform "$VALI_FPATH" --inplace=True --workers=16
    kwcoco conform "$TEST_FPATH" --inplace=True --workers=16

    kwcoco conform "$VALI_FPATH" --inplace=True --workers=16
    kwcoco tables "$VALI_FPATH" -g1
fi

#KWCOCO_BUNDLE_DPATH=$HOME/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH
#kwcoco conform "$KWCOCO_BUNDLE_DPATH"/train-v04.kwcoco.zip "$KWCOCO_BUNDLE_DPATH"/train-v04.mscoco.json --legacy=True
#kwcoco conform "$KWCOCO_BUNDLE_DPATH"/vali-v04.kwcoco.zip "$KWCOCO_BUNDLE_DPATH"/vali-v04.mscoco.json --legacy=True
#kwcoco conform "$KWCOCO_BUNDLE_DPATH"/test-v04.kwcoco.zip "$KWCOCO_BUNDLE_DPATH"/test-v04.mscoco.json --legacy=True

KWCOCO_BUNDLE_DPATH=$HOME/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH
DVC_DATA_DPATH=$HOME/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH
KWCOCO_BUNDLE_DPATH=$DVC_DATA_DPATH

#INPUT_DSET=$KWCOCO_BUNDLE_DPATH/data.kwcoco.zip
NEWSTYLE_DSET=$KWCOCO_BUNDLE_DPATH/data-with-polys.kwcoco.zip
OLDSTYLE_DSET=$KWCOCO_BUNDLE_DPATH/data-with-closed-polys.kwcoco.zip
kwcoco conform "$NEWSTYLE_DSET" "$OLDSTYLE_DSET" --legacy=True

# Manual cleanup, remove bad annotations
python -c "if 1:
import kwcoco
import kwimage
dset = kwcoco.CocoDataset('/home/joncrall/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH/data-with-closed-polys.kwcoco.zip')

to_remove = []
for ann in dset.dataset['annotations']:
    ann.pop('poly', None)
    if 'segmentation' not in ann:
        to_remove.append(ann)
    if ann['area'] < 1:
        to_remove.append(ann)
dset.remove_annotations(to_remove, verbose=3)
dset.dump()

# Test that the pycocotools interface works
coco = dset._aspycoco()
for ann in coco.dataset['annotations']:
    coco.annToRLE(ann)
"

kwcoco validate "$OLDSTYLE_DSET" --workers 10

#INPUT_DSET=$RAW_DSET
INPUT_DSET=$OLDSTYLE_DSET

FULL_DSET=$KWCOCO_BUNDLE_DPATH/data-v05-noscallop.kwcoco.zip
LEARN_FPATH=$KWCOCO_BUNDLE_DPATH/learn-v05-noscallop.kwcoco.zip
TRAIN_FPATH=$KWCOCO_BUNDLE_DPATH/train-v05-noscallop.kwcoco.zip
VALI_FPATH=$KWCOCO_BUNDLE_DPATH/vali-v05-noscallop.kwcoco.zip
TEST_FPATH=$KWCOCO_BUNDLE_DPATH/test-v05-noscallop.kwcoco.zip

#kwcoco stats "$KWCOCO_BUNDLE_DPATH"/data-with-polys.kwcoco.zip


kwcoco modify_categories --remove_empty_images=True \
   --keep "
- Jonah or rock crab
- red hake
- winter-little skate
- unknown/other cephalopod
- unknown/other shark
- yellowtail flounder
- American lobster
- windowpane flounder
- barndoor skate
- ocean pout
- spiny dogfish
- monkfish
- gadid
- sculpin/grubby/sea raven
- madmade/trash
- squid
- unknown/other skate
- unknown/other crab
- fourspot flounder
- unknown/other decapod
- winter-little skate
- moon snail
- unknown/other fish
- unknown/other gastropod
- unknown/other invert
- unknown/other flatfish
- unknown/other bivalve
- whelk
- red hake
- Jonah or rock crab
- unknown/other hake
- hermit crab
- eel-like fish
" --src "$INPUT_DSET" --dst "$FULL_DSET"
kwcoco stats "$FULL_DSET"
#kwcoco show "$FULL_DSET"

kwcoco split "$FULL_DSET" --rng 10621406 --dst1 "$LEARN_FPATH" --dst2 "$TEST_FPATH"
kwcoco split "$LEARN_FPATH" --rng 10631407 --dst1 "$TRAIN_FPATH" --dst2 "$VALI_FPATH"

cd "$KWCOCO_BUNDLE_DPATH"
kwcoco stats "$TRAIN_FPATH" "$VALI_FPATH" "$TEST_FPATH"


python -c "if 1:
import kwcoco
import kwimage
dset = kwcoco.CocoDataset('/home/joncrall/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH/data-with-closed-polys.kwcoco.zip')

missing = 0
for ann in dset.dataset['annotations']:
    if 'segmentation' not in ann:
        missing += 1
    ann.pop('poly', None)
dset.dump()

for ann in dset.annots().objs_iter():
    sseg = kwimage.Segmentation.coerce(ann['segmentation'])
    sseg.to_coco('orig')

tmp = dset.subset([3]).copy()
tmp.conform(legacy=True)
from kwcoco.compat_dataset import COCO

coco = tmp._aspycoco()
coco.annToMask(coco.dataset['annotations'][1])

import kwimage
from kwcoco import coco_schema
poly = kwimage.Polygon.random().to_coco(style='orig')
mpoly = kwimage.MultiPolygon.random().to_coco(style='orig')

coco_schema.MSCOCO_POLYGON.validate(poly)
coco_schema.MSCOCO_MULTIPOLYGON.validate(mpoly)


    ...
"


### Check if the train / val / test splits for mask no-mask are comparable
### (I think they aren't. Yup, they are different... Blegh)
### The purpose of this script is now to resplit the v05 data into v06 to make
### it compatible with v04.
python -c "if 1:

    import kwcoco
    import rich
    import ubelt as ub
    bundle_dpath = ub.Path('~/data/dvc-repos/viame_dvc/private/Benthic/HABCAM-FISH').expand()
    splits_paths = {}
    splits_paths['v04'] = {
        'data': (bundle_dpath / 'data-v04-noscallop.kwcoco.zip'),
        'val': (bundle_dpath / 'vali-v04-noscallop.kwcoco.zip'),
        'train': (bundle_dpath / 'train-v04-noscallop.kwcoco.zip'),
        'test': (bundle_dpath / 'test-v04-noscallop.kwcoco.zip'),
        'learn': (bundle_dpath / 'learn-v04-noscallop.kwcoco.zip'),
    }
    splits_paths['v05'] = {
        'data': (bundle_dpath / 'data-v05-noscallop.kwcoco.zip'),
        'val': (bundle_dpath / 'vali-v05-noscallop.kwcoco.zip'),
        'train': (bundle_dpath / 'train-v05-noscallop.kwcoco.zip'),
        'test': (bundle_dpath / 'test-v05-noscallop.kwcoco.zip'),
        'learn': (bundle_dpath / 'learn-v05-noscallop.kwcoco.zip'),
    }
    all_paths = list(splits_paths['v04'].values()) + list(splits_paths['v05'].values())
    all_dsets = list(kwcoco.CocoDataset.load_multiple(all_paths, workers=8))
    fpath_to_dset = {d.fpath: d for d in all_dsets}
    splits = {}
    for v in splits_paths.keys():
        splits[v] = {k: fpath_to_dset[p] for k, p in splits_paths[v].items()}

    names = {}
    for v in splits.keys():
        names[v] = {
            k: splits[v][k].images().lookup('file_name')
            for k in splits[v].keys()
        }

    v1 = 'v04'
    v2 = 'v05'
    # Demonstrate there is a problem
    for k in ['train', 'val', 'test', 'learn']:
        overlaps = xd.set_overlaps(names[v1][k], names[v2][k], k + '1', k + '2')
        has_cross_version_problem = overlaps['isect'] != overlaps['union']
        print('----')
        rich.print(f'{has_cross_version_problem=}')
        print(ub.urepr(overlaps, nl=1))

    import itertools as it
    # Check that at least within the versions that everything is ok.
    # That looks good
    for v in ['v04', 'v05']:
        print(f'-- {v} --')
        for k1, k2 in it.combinations(['train', 'val', 'test'], 2):
            print(f'--{k1} {k2}--')
            overlaps = xd.set_overlaps(names[v][k1], names[v][k2], k1, k2)
            has_internal_problem = overlaps['isect'] != 0
            rich.print(f'{has_internal_problem=}')
            print(ub.urepr(overlaps, nl=1))

    # Check no-mask assumption
    for k1, dset1 in splits['v04'].items():
        no_ssegs1 = all(s is None for s in dset1.annots().lookup('segmentation', None))
        rich.print(f'{k1=} {no_ssegs1=}')
    # Check mask assumption
    for k2, dset2 in splits['v05'].items():
        all_ssegs2 = not any(s == None for s in dset2.annots().lookup('segmentation', None))
        rich.print(f'{k2=} {all_ssegs2=}')

    # The v04 and v05 datasets should at least have the same image base
    assert names['v04']['data'] == names['v05']['data']
    assert splits['v04']['data'].images().objs == splits['v05']['data'].images().objs

    # Now create a v06 that matches v04
    split_gids = {}
    split_gids['train'] = list(splits['v04']['train'].images())
    split_gids['val'] = list(splits['v04']['val'].images())
    split_gids['test'] = list(splits['v04']['test'].images())

    splits6 = {}
    for k, gids in split_gids.items():
        new_dset = splits['v05']['data'].subset(image_ids=gids)
        new_dset.fpath = bundle_dpath / f'{k}-v06-noscallop.kwcoco.zip'
        splits6[k] = new_dset
    v = 'v06'
    splits[v] = splits6
    names[v] = {
        k: splits[v][k].images().lookup('file_name')
        for k in splits[v].keys()
    }

    v1 = 'v04'
    v2 = 'v06'
    # Demonstrate there is no longer a problem
    for k in ['train', 'val', 'test']:
        overlaps = xd.set_overlaps(names[v1][k], names[v2][k], k + '1', k + '2')
        has_cross_version_problem = overlaps['isect'] != overlaps['union']
        print('----')
        rich.print(f'{has_cross_version_problem=}')
        print(ub.urepr(overlaps, nl=1))

    for k, dset in splits6.items():
        dset.dump()
        ...


"

# Move data over to namek
rsync -avprPR --include="*/" --include="*.kwcoco.zip" --include="*.kwcoco.zip" --exclude="*" ~/data/dvc-repos/viame_dvc/private/Benthic/./HABCAM-FISH namek:data/dvc-repos/viame_dvc/private/Benthic


rsync -avprPR --include="*/*.kwcoco.zip" --include="*.kwcoco.zip" --exclude="*" /home/data/./dataset remote:data


echo "test" > test.txt
rsync test.txt namek:test.txt
